//
//  ViewController.swift
//  Praise
//
//  Created by Abdo Emad on 14/04/2024.
//

import UIKit
import AVFoundation
import AudioToolbox

class ViewController: UIViewController {
    
    
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var vibrationButton: UIButton!
    @IBOutlet weak var playSoundButton: UIButton!
    @IBOutlet weak var countButton: UIButton!
    @IBOutlet weak var targetField: UITextField!
    @IBOutlet weak var numLable: UILabel!
    @IBOutlet weak var totalLable: UILabel!
    
    @IBOutlet weak var thirtyThreeLable: UILabel!
    @IBOutlet weak var zeroLable: UILabel!
    
    
    @IBOutlet weak var stackViewOne: UIStackView!
    @IBOutlet weak var stackViewTwo: UIStackView!
    
    @IBOutlet weak var zeroStation: UILabel!
    @IBOutlet weak var secondNumLable: UILabel!
    
    @IBOutlet weak var ChangeColorButton: UIButton!
    
    @IBOutlet weak var resetButton: UIButton!
    
    //MARK: - Properties
    
    let number = "savedNumber"
    let targetNum = "targetNumber"
    let totalNum = "totalNumber"
    let secondNum = "secondNumber"
    let stationNum = "stationNumber"
    let station = "station"
    let vibrate = "vibrate"
    let sound = "sound"
    let image = "savedImage"
    var ShouldVibrate: Bool = true
    var shouldPlaySound: Bool = true
    var firstStation : Bool = true
    var currentNummber = 0
    var currentIndex = 0
    var totalCount = 0.0
    var player : AVAudioPlayer?
    var generator: UIImpactFeedbackGenerator?
    var hasLanched = false
    var imageArray : [Images] = [
        Images(image: UIImage(named: "image30")!),
        Images(image: UIImage(named: "image31")!),
        Images(image: UIImage(named: "image32")!),
        Images(image: UIImage(named: "image33")!),
        Images(image: UIImage(named: "image34")!),
        Images(image: UIImage(named: "image35")!),
        Images(image: UIImage(named: "image36")!),
        Images(image: UIImage(named: "image37")!),
        Images(image: UIImage(named: "image38")!),
        Images(image: UIImage(named: "image39")!),
        Images(image: UIImage(named: "image40")!)
        
    ]
    //MARK: -  lifeCyclie
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        loadNumbers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hasLanched = true
        loadBoolean()
    }
    //MARK: -  SetUp
    
    private func setUp(){
        targetField.delegate = self
        addDoneButtonToTextField()
        vibrationButton.imageView?.layer.cornerRadius = vibrationButton.frame.height / 2
        playSoundButton.imageView?.layer.cornerRadius = vibrationButton.frame.height / 2
        ChangeColorButton.layer.cornerRadius = vibrationButton.frame.height / 2
        targetField.isEnabled = firstStation
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        longPressGesture.minimumPressDuration = 0.5
        resetButton.addGestureRecognizer(longPressGesture)
        
    }
    @objc func handleLongPress(_ gesture : UILongPressGestureRecognizer){
        if gesture.state == .began {
            resetNumbers()
            PlayVibration()
        }
    }
    //MARK: - Load Stored Data
    
    func loadNumbers(){
        let savedNumber = UserDefaults.standard.integer(forKey: number)
        let targetNum = UserDefaults.standard.integer(forKey: targetNum)
        let totalNumber = UserDefaults.standard.double(forKey: totalNum)
        let secondNumber = UserDefaults.standard.integer(forKey: secondNum)
        let stationNumber = UserDefaults.standard.integer(forKey: stationNum)
        numLable.text = "\(savedNumber)"
        targetField.text = "\(targetNum)"
        totalLable.text = "\(totalNumber)"
        secondNumLable.text = "\(secondNumber)"
        zeroStation.text = "\(stationNumber)"
        
        if let imageData = UserDefaults.standard.data(forKey: image){
            let loadedImage = UIImage(data: imageData)
            
            imageView.image = loadedImage
        }
    }
    
    private func loadBoolean(){
        let station = UserDefaults.standard.bool(forKey: station)
        let sound = UserDefaults.standard.bool(forKey: sound)
        let vibrate = UserDefaults.standard.bool(forKey: vibrate)
        
        shouldPlaySound = sound
        ShouldVibrate = vibrate
        firstStation = station
        
        changeStation()
        checkPlaySound()
        checkVibration()
    }
    
    //MARK: -  CheckingOut
    
    private func changeStation(){
        if firstStation {
            zeroLable.textColor  = .red
            thirtyThreeLable.textColor = .white
            stackViewOne.isHidden = false
            stackViewTwo.isHidden = true
            targetField.text = "0"
            targetField.textColor = .black
            targetField.font = .systemFont(ofSize: 25)
        }else{
            zeroLable.textColor  = .white
            thirtyThreeLable.textColor = .red
            stackViewOne.isHidden = true
            stackViewTwo.isHidden = false
            targetField.text = "Not Available "
            targetField.textColor = .red
            targetField.font = .systemFont(ofSize: 10)
            
        }
    }
    
    private func checkPlaySound(){
        if shouldPlaySound {
            playSoundButton.setImage(UIImage(named: "image1"), for: .normal)
        }else{
            playSoundButton.setImage(UIImage(named: "image2"), for: .normal)
        }
    }
    
    private func checkVibration(){
        guard hasLanched else {
            return
        }
        
        if ShouldVibrate{
            generator = UIImpactFeedbackGenerator(style: .medium)
            generator?.impactOccurred()
            vibrationButton.setImage(UIImage(named: "image"), for: .normal)
        }else{
            vibrationButton.setImage(UIImage(named: "image4"), for: .normal)
            generator = nil
        }
        
    }
    
    func resetNumbers(){
        if firstStation{
            UserDefaults.standard.set(0, forKey: number)
            UserDefaults.standard.setValue(0, forKey: targetNum)
            numLable.text = "0"
            targetField.text = "0"
        }else{
            UserDefaults.standard.setValue(0, forKey: secondNum)
            UserDefaults.standard.setValue(0, forKey: stationNum)
            secondNumLable.text = "0"
            zeroStation.text = "0"
        }
        
    }
    
    private func resetAlert(){
        
        let reset = UIAlertAction(title: "Reset", style: .destructive) { _ in
            self.resetNumbers()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        showAlert(title: "Are you sure you want to reset the counter?", message: "Long press on reset button to reset without warning.", actions: [reset, cancelAction], viewController: self, style: .alert)
    }
    
    //MARK: -  Actions
    @IBAction func resetButton(_ sender: Any) {
        
        if firstStation {
            if numLable.text != "0"{
                resetAlert()
            }
        }else{
            if secondNumLable.text != "0" || zeroStation.text != "0"{
                resetAlert()
            }
        }
        
        PlayVibration()
    }

    @IBAction func increaseButton(_ sender: Any) {
        if firstStation {
            currentNummber = UserDefaults.standard.integer(forKey: number)
            currentNummber += 1
            UserDefaults.standard.setValue(currentNummber, forKey: number)
            totalCount = UserDefaults.standard.double(forKey: totalNum)
            totalCount += 1
            UserDefaults.standard.setValue(totalCount, forKey: totalNum)
            
            numLable.text = "\(currentNummber)"
            totalLable.text = "\(totalCount)"
            
            if currentNummber == UserDefaults.standard.integer(forKey: targetNum){
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                resetNumbers()
            }
            
        }else{
            
            var secondNumber = UserDefaults.standard.integer(forKey: secondNum)
            secondNumber += 1
            UserDefaults.standard.setValue(secondNumber, forKey: secondNum)
            secondNumLable.text = "\(secondNumber)"
            
            totalCount = UserDefaults.standard.double(forKey: totalNum)
            totalCount += 1
            UserDefaults.standard.setValue(totalCount, forKey: totalNum)
            totalLable.text = "\(totalCount)"
            
            if secondNumber == 33{
                var stationNumber =  UserDefaults.standard.integer(forKey:stationNum)
                stationNumber += 1
                UserDefaults.standard.setValue(stationNumber, forKey: stationNum)
                UserDefaults.standard.setValue(0, forKey: secondNum)
                secondNumLable.text = "\(0)"
                zeroStation.text = "\(stationNumber)"
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            }
        }
        PlaySound(soundName:"click")
        PlayVibration()
    }
    
    @IBAction func resetTotal(_ sender: Any) {
        if totalLable.text != "0"{
            
            let resetAction = UIAlertAction(title: "Reset", style: .destructive) { _ in
                UserDefaults.standard.set(0, forKey: self.totalNum)
                self.totalLable.text = "0"
            }
            let cancelActoin = UIAlertAction(title: "Cancel", style: .cancel)
            
            showAlert(title: "Are you sure you want reset the total count ?", message: "", actions: [resetAction,cancelActoin], viewController: self, style: .alert)
        }
        
        PlayVibration()
        
    }
    
    @IBAction func VibrationButtonClicked(_ sender: UIButton) {
        ShouldVibrate.toggle()
        checkVibration()
        PlayVibration()
        UserDefaults.standard.setValue(ShouldVibrate, forKey: vibrate)
    }
    
    @IBAction func playClicSoundButtonPresed(_ sender: Any) {
        shouldPlaySound.toggle()
        checkPlaySound()
        PlayVibration()
        UserDefaults.standard.setValue(shouldPlaySound, forKey: sound)
    }
    
    
    // Change Station from counting normal  to 33
    
    @IBAction func changeStationButton(_ sender: Any) {
        firstStation.toggle()
        changeStation()
        PlayVibration()
        
        UserDefaults.standard.set(firstStation, forKey: station)
        targetField.isEnabled = firstStation
    }
    
    
    @objc func doneButtonTapped(){
        if let text = targetField.text , let number = Int(text){
            UserDefaults.standard.setValue(number, forKey: targetNum)
            PlayVibration()
        }
        targetField.resignFirstResponder()
    }
    
    
    @IBAction func changeColorButton(_ sender: Any) {
        
             let currentImage = imageArray[currentIndex].image

              if let imageData = currentImage.pngData() {
                  UserDefaults.standard.set(imageData, forKey: image)
              }
              imageView.image = currentImage
              currentIndex = (currentIndex + 1) % imageArray.count
              PlayVibration()
          
    }
    //MARK: - soundPlayer
    
    func PlaySound(soundName: String){
        if shouldPlaySound{
            if let path = Bundle.main.path(forResource: soundName, ofType: "mp3"){
                let url = URL(fileURLWithPath: path)
                
                do {
                    player = try AVAudioPlayer(contentsOf: url)
                    player?.prepareToPlay()
                    player?.play()
                }catch (let error){
                    print("error:\(error.localizedDescription)")
                }
            }else{
                print("can't find the file ")
            }
            
        }else{
            player?.stop()
        }
        
    }
    
    func PlayVibration(){
        
        checkVibration()
    }
}

//MARK: -  TextFiedl


extension ViewController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.text = ""
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.text == "" {
            textField.text = "0"
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let text = targetField.text , let number = Int(text){
            UserDefaults.standard.setValue(number, forKey: targetNum)
        }
        targetField.resignFirstResponder()
    }
    
//     addDoneButton
    
    func addDoneButtonToTextField() {
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonTapped))
        
        toolbar.setItems([doneButton], animated: false)
        
        targetField.inputAccessoryView = toolbar
    }
    
}


//MARK: - Alerts

extension ViewController{
    
    func showAlert(title: String, message: String, actions: [UIAlertAction], viewController: UIViewController, style: UIAlertController.Style) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        
        for action in actions {
            alertController.addAction(action)
        }
        viewController.present(alertController, animated: true, completion: nil)
    }
    
}




extension UIView{
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
    }
}

struct Images {
    var image : UIImage
}
